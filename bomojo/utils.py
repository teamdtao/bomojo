from collections import OrderedDict
from decimal import Decimal
import hashlib
import re

from django.conf import settings

from bomojo import defaults

_money_quantization = Decimal('1.00')
_whitespace_pattern = re.compile(r'\s+')


def get_avatar_url(email, size=None, default=None):
    email = email.lower()
    size = size or get_setting('DEFAULT_AVATAR_SIZE')
    default = default or get_setting('DEFAULT_AVATAR_STYLE')

    digest = hashlib.new('md5', email.encode('utf-8')).hexdigest()
    return 'https://gravatar.com/avatar/%(digest)s?size=%(size)d&d=%(default)s' % {
        'digest': digest,
        'size': size,
        'default': default
    }


def get_setting(setting_name):
    return getattr(settings, setting_name, getattr(defaults, setting_name))


def deduplicate(values):
    """Returns a list of the same values but with duplicates removed

    Unlike list(set(values)), this function preserves the ordering of the
    original list.
    """
    return list(OrderedDict.fromkeys(values))


def split_on_whitespace(text):
    return _whitespace_pattern.split(text)


def format_money(value):
    if not isinstance(value, Decimal):
        value = Decimal(value)
    return value.quantize(_money_quantization)
