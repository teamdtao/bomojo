# bomojo

A Django application providing models and APIs for movie box office data based
on [pybomojo][1].

[1]: https://bitbucket.org/teamdtao/pybomojo
